# Que son los indices
Un índice de la base de datos es una estructura de datos que mejora la velocidad 
de las operaciones en una tabla. Tabien que ayuda a diferenciar entre los campos
que estan siendo insertados.

¿Por que usar indices?
Los índices mejoran el tiempo de recuperación de los datos en la consulta 
realizadas contra nuestra base de datos.

Desventajas:
la creación de índices implica un aumento en el tiempo de ejecución 
sobre aquellas consultas de inserción, actualización y eliminación 
realizadas sobre los datos afectados por el índice.
Tambien de tiene que tener un espacio en la memoria.
##Tipos de indices
PRIMARY KEY: Este índice se ha creado para generar consultas especialmente rápidas, 
debe ser único y no se admite el almacenamiento de NULL.

KEY o INDEX: Son usados indistintamente por MySQL, 
permite crear indices sobre una columna, sobre varias columnas o sobre partes de una columna.
    
UNIQUE: Este tipo de índice no permite el almacenamiento de valores iguales.

FULLTEXT: Permiten realizar búsquedas de palabras. 
Sólo pueden usar en columnsas CHAR, VARCHAR o TEXT.
    
SPATIAL: Este tipo de índices solo puede usarse en columnas de datos 
geométricos (spatial) y en el motor MyISAM.

### indices
CREATE TABLE `ventas` (
	`id_venta` INT NULL AUTO_INCREMENT,
	`cantidad` INT NULL,
	`producto` INT NULL
)
;
/* Error de SQL (1075): Incorrect table definition; there can be only one auto column and it must be defined as a key */


CREATE TABLE `ventas` (
	`id_venta` INT NULL AUTO_INCREMENT,
	`cantidad` INT NULL,
	`producto` INT NULL,
	INDEX `main` (`id_venta`)
)trando a la sesión "Abril" */
SHOW CREATE TABLE `actividad_trigers`.`ventas`;
/* Error de SQL (1064): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'a la sesión "Abril" */
SHOW CREATE TABLE `actividad_trigers`.`ventas`' at line 6 */
