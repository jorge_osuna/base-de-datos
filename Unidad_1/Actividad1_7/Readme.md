# Vistas
#### Descripcion:
na vista es una consulta que se presenta como una tabla (virtual) a partir de un
conjunto de tablas en una base de datos relacional.
Las vistas tienen la misma estructura que una tabla: filas y columnas. La única
diferencia es que sólo se almacena de ellas la definición, no los datos. 

Ventajas:

    Facilita el manejo de grandes volúmenes de información, haciéndolos mas fáciles 
    y rápidos de manipular.
    Brinda mayor seguridad a la información.
    Evita la redundancia de la información.
    Mejora la metodología de trabajo, se hace mas organizada.
    Facilita la realización de consultas en la BD por lo que se facilita la toma de decisiones.
    
Desventajas:

    Rendimiento ya que las vistas crean una apariencia de tabla por lo que el SMBD 
    debe traducir las consultas definidas en una vista.
    restricciones de actualización, cuando se solicita la actualización de una fila 
    de la vista se debe traducir en una petición de actualización a la tabla de origen
    de la vista, esto en vistas muy grandes puede causar problemas y evitar que
    las actualizaciones se realicen.



## ejemplo

CREATE VIEW vista_cientificos AS
(
SELECT * FROM cientificos;
)

SELECT * FROM vista_cientificos;
Genera el select de los usuarios salvados en vista_cientificos.

CREATE VIEW vita_cientificos_asignados
(
SELECT DNI,NomApels FROM cientificos WHERE 1 <
(SELECT COUNT(*) FROM asignado_a WHERE cientifico=cientificos.DNI)
AND 80 <
(SELECT AVG(Horas) FROM proyectos INNER JOIN asignado_a ON proyectos.id = asignado_a.proyecto WHERE cientifico = cientificos.DNI);
)
SELECT * FROM vista_cientificps_asignados;
genera un select de los cientificos con los proyectos con subconsultas

