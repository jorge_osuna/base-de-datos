# universidad tecnologica de hermosillo


>     alumno: Jorge Eduador Osuna Castro
>  
>     materiea: Base de datos para aplicaciones
-------------------------------------------------------------------------------- 
### introducción

lo que se vera son los mapas conceptuales de Conceptos del Modelo Relacional:
Aplicación de BD, Modelado de datos, Modelo Relacional
Diferencias entre relacional y no relacional, Que es un SGBD
Mejores SGBD mas usados, Funciones de los SGBD, Comparativa de SBGB mas comunes
Tipos de DB, Restricciones Relacionales, Integridad de entidades e integridad
referencial,Transformación de E- R a Modelo Relacional.

## Mapa conceptual

#### 1.-coceptos de modelo relacional


![aplicacionBD](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/raw/master/Unidad_1/Actividad1_1/img/aplicacionBD.png)

#### 2.-diferencia entre relacional y no relacional

![diferencia](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/raw/master/Unidad_1/Actividad1_1/img/diferencia.png)

#### 3.-sistema de gestion de base de datos

![sistema_de_gestion](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/raw/master/Unidad_1/Actividad1_1/img/sistema_de_gestio.png)

#### 4.-mejores gestiondes de base de datos

![gestores_BD](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/raw/master/Unidad_1/Actividad1_1/img/gestores_BD.png)


#### 5.-funcion de los gestores de base de datos

![funcion](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/raw/master/Unidad_1/Actividad1_1/img/funcion.png)

#### 6.-comparativa de SBGB mas comunes
![Comparativa-de-SBGB-mas-comunes](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/raw/master/Unidad_1/Actividad1_1/img/Comparativa-de-SBGB-mas-comunes.png)

#### 7.-Tipo de BD
![TipoBD](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/raw/master/Unidad_1/Actividad1_1/img/TipoBD.png)

#### 8.-Restricciones relacionales
![Restricciones_relaciones](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/raw/master/Unidad_1/Actividad1_1/img/Restricciones_relacionales.png)

#### 9.-integridad de entidad y relacional
![integridad](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/raw/master/Unidad_1/Actividad1_1/img/integridad.png)

### 10.-converir modelo entidad relacional a modelo relacional
![Captura](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/raw/master/Unidad_1/Actividad1_1/img/Captura.PNG)
![Captura1](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/raw/master/Unidad_1/Actividad1_1/img/Captura1.PNG)

# actividad 1.1a 
![diagramaRelaciona](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/raw/master/Unidad_1/Actividad1_1/img/diagramaRelaciona.PNG)