# universidad tecnologica de hermosillo
### Caso práctico de elaboración de un diccionario de datos
#### Alumno:
#### Jorge eduardo osuna
# introduccion
En este tema veremos como se construye un diccionario de datos y como
es un diagrama de flujo de datos,tambien veremos las difiniciones.
--------------------------------------------------------------------------------

#### Diccionario de datos
Un diccionario de datos es un conjunto de definiciones  que contiene las
características lógicas y puntuales de los datos que se van a utilizar
en el sistema que se programa, incluyendo nombre, descripción, alias, contenido
y organización.
#### Diagrama de flujo
Un diagrama de flujo de datos es una representación gráfica del flujo de datos
a través de un sistema de información. Un diagrama de flujo de datos también se
puede utilizar para la visualización de procesamiento de datos. 
Es una práctica común para un diseñador dibujar un contexto a nivel de DFD que 
primero muestra la interacción entre el sistema y las entidades externas

## Diccionario de Datos
![Diccionario de datos](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/blob/master/Unidad_1/Actividad1_2/img/dic.pdf)
## Diagrama de flujo de datos
![diagrama](https://gitlab.com/tic18311062/base_de_datos_para_aplicaciones/blob/master/Unidad_1/Actividad1_2/img/diagramaDatosNuevo.png)
