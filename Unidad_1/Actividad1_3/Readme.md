# universidad tecnologica de hermosillo
### alumno: jorge eduardo
#### Actividad 1.3
--------------------------------------------------------------------------

### Tema:
#### Caso práctico de elaboración de restricciones de las DB de unicidad, referencial y de dominio.

## introduccion.

en este tema veremos las restricciones que nos permite poner en base de datos
como se usan como se ponene y sus definiciones

##### NOT NULL
Establece la obligatoriedad de que esta columna tenga un valor no nulo.
Se debe especificar junto a la columna a la que afecta. Los valores nulos
no ocupan espacio, y son distintos a 0 y al espacio en blanco. Hay que tener
cuidado con los valores nulos en las operaciones, ya que 1 * NULL es igual a NULL.

#####  UNIQUE
Evita valores repetidos en una columna, admitiendo valores nulos.
Oracle crea un índice automáticamente cuando se habilita esta restricción
y lo borra al deshabilitarse.

##### DEFAULT
Establece un valor por defecto para esa columna, si no se le asigna ninguno.

##### CHECK
Comprueba que se cumpla una condición determinada al rellenar esa columna. 
Esta condición sólo debe estar construida con columnas de esta misma tabla.

##### PRIMARY KEY
Establece el conjunto de columnas que forman la clave primaria de esa tabla.
Se comporta como única y obligatoria sin necesidad de explicitarlo. Sólo puede
existir una clave primaria por tabla. Puede ser referenciada como clave ajena 
por otras tablas. Crea un índice automáticamente cuando se habilita o se crea 
esta restricción. En Oracle, los índices son construidos sobre árboles B+.

##### FOREIGN KEY
Establece que el contenido de esta columna será uno de los valores contenidos
en una columna de otra tabla maestra. Esta columna marcada como clave ajena
puede ser NULL. No hay límite en el número de claves ajenas. La clave ajena 
puede ser otra columna de la misma tabla. Se puede forzar que cuando una fila
de la tabla maestra sea borrada, todas las filas de la tabla detalle cuya clave
ajena coincida con la clave borrada se borren también. Esto se consigue añadiendo
la coletilla ON DELETE CASCADE en la definición de la clave ajena.

### Restriccion Relacional

En esta parte se vera como se hace una tabla con sus campos 
y como se pone Primary key

CREATE TABLE \`almacen\` (
	\`id\` INT(11) NOT NULL,
	\`nombre\` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_spanish2_ci',
	\`cantidad\` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (\`id\`)
)
COLLATE='utf8_spanish2_ci'
ENGINE=InnoDB

si primero creaste la tabla y no pusiste la primary key puedes poner este comando


ALTER TABLE \`almacen\`
	ADD PRIMARY KEY (\`id\`);

para hacer un foren key se hace de esta forma

SHOW CREATE TABLE \`practica\`.\`venta\`;
SHOW CREATE TABLE \`practica\`.\`almacen\`;
SHOW CREATE TABLE \`practica\`.\`venta\`;
SELECT 1 FROM \`almacen\` LIMIT 1;
SHOW COLUMNS FROM \`almacen\`;
SHOW CREATE TABLE \`practica\`.\`almacen\`;
ALTER TABLE \`venta\`
	ADD CONSTRAINT \`FK_venta_almacen\` FOREIGN KEY (\`id_producto\`) REFERENCES \`almacen\` (\`id\`);
SELECT \`DEFAULT_COLLATION_NAME\` FROM \`information_schema\`.\`SCHEMATA\` WHERE \`SCHEMA_NAME\`='practica';
SHOW TABLE STATUS FROM \`practica\`;
SHOW FUNCTION STATUS WHERE \`Db\`='practica';
SHOW PROCEDURE STATUS WHERE \`Db\`='practica';
SHOW TRIGGERS FROM \`practica\`;
SELECT *, EVENT_SCHEMA AS \`Db\`, EVENT_NAME AS \`Name\` FROM information_schema.\`EVENTS\` WHERE \`EVENT_SCHEMA\`='practica';

#### error de insertar
INSERT INTO `practica`.`venta` (`id_venta`, `id_producto`) VALUES ('1', '5');
###### error
Error de SQL (1452): Cannot add or update a child row: a foreign key constraint fails (`practica`.`venta`, CONSTRAINT `FK_venta_almacen` FOREIGN KEY (`id_producto`) REFERENCES `almacen` (`id`))
#### error de update
UPDATE `practica`.`venta` SET `id_producto`='3' WHERE  `id_venta`=1 AND `id_producto`=1;
###### error
 Error de SQL (1452): Cannot add or update a child row: a foreign key constraint fails (`practica`.`venta`, CONSTRAINT `FK_venta_almacen` FOREIGN KEY (`id_producto`) REFERENCES `almacen` (`id`))
#### error delite
DELETE FROM `practica`.`venta` WHERE  `id_venta`=3 AND `id_producto`=1 LIMIT 1;
#### error
Error de SQL (1064): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near '=1 FROM `practica`.`venta` WHERE  `id_producto`=6' at line 1 
### VENTAJAS Y DESVENTAJAS
###### NOT NULL :
ventajas tiene que haber algo en el campo
desventajas no permite valores nulo
###### UNIQUE :
ventajas que siempre el valor tiene que ser unico no se puede repetir
desventajas no puedes repetir nada
###### DEFAULD :
ventajas siempre tendra un valor por defecto 
desventajas si no lo agregas no lo puedes cambiar
###### CHECK :
ventajas sirve para hacer verdaderos o falso
desventajas solo se puede con los valores de esa tabla
###### PRIMARY KEY :
ventajas sera unica y no permitira nulo 
desventajas solo pude haber un primary jey por tabla
###### FOREN KEY : 
ventajas permite agarrar los valores de otra tabla
desventajas no habra valor si no hay otro
#### SOFTWARE UTILIZADO
heidi