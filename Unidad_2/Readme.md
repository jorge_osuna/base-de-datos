# Universidad Tecnologica de Hermosillo

### Alumno:Jorge Osuna

####**Comandos**

START PROCEDURE
Empieza el procedimiento. todos los querries de modificacion despues del start
procedure tendran que tener un COMMIT para que den efecto.

COMMIT 
cuando se pone este comando, los efectos realizados son vistos para todos los
usuarios, ya afecta la base de datos.

ROLLBACK 
Todos los cambios son omitidos y la base de datos se devuelve a una version vieja.

####**Conceptos Transasiones**

Una transacción es una unidad de trabajo compuesta por diversas tareas, 
cuyo resultado final debe ser que se ejecuten todas o ninguna de ellas.

En otras palabra, los cambios realizados por el usuario en la base de datos,
no se realizaran hasta que el usuario no de COMMIT o se presente un error.

####**Niveles de Aislamiento**

Los niveles de aislamiento son:

SERIALIZABLE: No se permitirá a otras transacciones la inserción, 
actualización o borrado de datos utilizados por nuestra transacción. 
Los bloquea mientras dura la misma.

REPEATABLE READ: Garantiza que los datos leídos no podrán ser cambiados
por otras transacciones, durante esa transacción.


READ COMMITED: Una transacción no podrá ver los cambios de otras
conexiones hasta que no hayan sido confirmados o descartados.
    
READ UNCOMMITTED: No afectan los bloqueos producidos por otras
conexiones a la lectura de datos.
    
SNAPSHOT: Los datos seleccionados en la transacción se verán tal y
como estaban al comienzo de la transacción, 
y no se tendrán en cuenta las actualizaciones que hayan sufrido por
la ejecución de otras transacciones simultáneas.

####**Ventajas y Desventajas**

Ventajas:
    Se usan para procesos criticos.
    No hace cambios en caso de error.
    Son buenos para procesos criticos.
    Te permite checar los cambios a la base de datos antes de ejecutarlos.
    En caso de error se va a una version anterior de la base de datos.
    
Desventajas:
    Cuando cambio de version es mas complicado.
    No puede ser testable.


